import express from "express";
import { getFeedsPosts, getUserPosts, likePost } From "../controllers/posts.js";
import { verifyToken } from "../middleware/auth";

const router = express.Router();

/* READ */
router.get("/", verifyToken, getUserPosts);
router.get("/:userId/posts",verifyToken, getUserPosts);

/* UPDATE */
router.patch("/:zid/like", verifyToken, likePost);

export default router;