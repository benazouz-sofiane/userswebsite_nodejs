import express from "express";
import {
    getUser,
    getUsers,
    getUserFriends,
    addRemoveFriend
} from "../controllers/users.js";
import { verifyToken } from "../middleware/auth.js";
const userRoutes = express.Router();


/* Read */
userRoutes.get("/",verifyToken,getUsers);
userRoutes.get("/:id",verifyToken,getUser);
userRoutes.get("/:id/friends",verifyToken,getUserFriends);

/* UPDATE */
userRoutes.patch("/:id/:friendId", verifyToken,addRemoveFriend);

export default userRoutes;