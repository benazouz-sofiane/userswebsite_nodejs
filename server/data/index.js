import mongoose from "mongoose";
import faker from "faker";

const userId = [
    new mongoose.Types.ObjectId(),
    new mongoose.Types.ObjectId(),
    new mongoose.Types.ObjectId(),
    new mongoose.Types.ObjectId(),
    new mongoose.Types.ObjectId(),
    new mongoose.Types.ObjectId(),
    new mongoose.Types.ObjectId(),
    new mongoose.Types.ObjectId(),
];

// Génération aléatoire de données utilisateur
const generateUserData = () => {
    const firstName = faker.name.firstName();
    const lastName = faker.name.lastName();
    const email = faker.internet.email();
    const password = faker.internet.password(8);
    const picturePath = faker.image.avatar();
    const friends = [];
    const location = faker.address.city();
    const occupation = faker.name.jobTitle();
    const viewProfile = faker.random.number();
    const impressions = faker.random.number();

    return {
        firstName,
        lastName,
        email,
        password,
        picturePath,
        friends,
        location,
        occupation,
        viewProfile,
        impressions,
    };
};

// Génération de 10 exemples de données utilisateur
const generateUsersData = () => {
    const usersData = [];

    for (let i = 0; i < 10; i++) {
        const userData = generateUserData();
        usersData.push(userData);
    }

    return usersData;
};

// Utilisation de la fonction pour générer des exemples de données utilisateur
export const Users = generateUsersData();


